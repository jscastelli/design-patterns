#ifndef SETTEMPERATURECOMMAND_HPP
#define SETTEMPERATURECOMMAND_HPP

#include "ICommand.hpp"
#include "../Receivers/Thermostat.hpp"

class SetTemperatureCommand : public ICommand
{
	public:
		SetTemperatureCommand(Thermostat* aThermostat, double aTemperature);
		~SetTemperatureCommand();

		virtual void Execute();
		virtual void UnExecute();

	private:
		
		Thermostat* mThermostat{ nullptr };
		double mTargetTemperature{ 0.0 };
		double mPreviousTemperature{ 0.0 };
};

#endif