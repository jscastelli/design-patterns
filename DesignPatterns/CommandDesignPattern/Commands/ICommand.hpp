#ifndef ICOMMAND_HPP
#define ICOMMAND_HPP

#include <vector>

class ICommand
{
	public:
		virtual void Execute() = 0;
		virtual void UnExecute() = 0;
};

#endif