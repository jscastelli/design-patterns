#include "TurnOffLightCommand.hpp"

TurnOffLightCommand::TurnOffLightCommand(Light* aLight)
{
   mLight = aLight;
}

TurnOffLightCommand::~TurnOffLightCommand()
{
}

void TurnOffLightCommand::Execute()
{
   mLight->TurnOff();
}

void TurnOffLightCommand::UnExecute()
{
   mLight->TurnOn();
}
