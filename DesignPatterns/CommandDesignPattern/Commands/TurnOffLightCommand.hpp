#ifndef TURNOFFLIGHTCOMMAND_HPP
#define TURNOFFLIGHTCOMMAND_HPP

#include "ICommand.hpp"

#include "../Receivers/Light.hpp"

class TurnOffLightCommand : public ICommand
{
	public:
		TurnOffLightCommand(Light* aLight);
		~TurnOffLightCommand();

		virtual void Execute();
		virtual void UnExecute();

	private:
		
		Light* mLight{ nullptr };
};

#endif