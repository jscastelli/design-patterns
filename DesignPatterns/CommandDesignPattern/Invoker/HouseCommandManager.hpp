#ifndef HouseCommandManager_HPP
#define HouseCommandManager_HPP

//Commands available
#include "../Commands/ICommand.hpp"
#include "../Commands/TurnOnLightCommand.hpp"
#include "../Commands/TurnOffLightCommand.hpp"
#include "../Commands/SetTemperatureCommand.hpp"

//The receiver of commands
#include "../Receivers/House.hpp"

//STD
#include <vector>
#include <string>
#include <memory>

class HouseCommandManager
{
	public:
		HouseCommandManager(House* aHouse);
		~HouseCommandManager();

		void TurnOnKitchenLight();
		void TurnOffKitchenLight();
		void TurnOnBathroomLight();
		void TurnOffBathroomLight();
		void SetTemperature(double aTemperature);

		void UndoCommand();
		void RedoCommand();

	private:

	   void ExecuteCommand(std::shared_ptr<ICommand> aCommand);

		//Used to keep track of the command history and undo/redo them as specified
		std::vector <std::shared_ptr<ICommand>> mUndoHistory{};
		std::vector <std::shared_ptr<ICommand>> mRedoHistory{};

		//The receiver in which all commands will act upon
		House* mHouse{ nullptr };
};

#endif