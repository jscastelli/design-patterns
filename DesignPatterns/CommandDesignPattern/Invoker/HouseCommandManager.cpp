#include "HouseCommandManager.hpp"
#include "../Commands/SetTemperatureCommand.hpp"
#include <iostream>

HouseCommandManager::HouseCommandManager(House* aHouse)
{
	mHouse = aHouse;
}

HouseCommandManager::~HouseCommandManager()
{
}

void HouseCommandManager::TurnOnKitchenLight()
{
   std::cout << "Turn On Kitchen Light" << std::endl;
   ExecuteCommand(std::shared_ptr<ICommand>(new TurnOnLightCommand(&mHouse->mKitchenLight)));
}

void HouseCommandManager::TurnOffKitchenLight()
{
   std::cout << "Turn Off Kitchen Light" << std::endl;
   ExecuteCommand(std::shared_ptr<ICommand>(new TurnOffLightCommand(&mHouse->mKitchenLight)));
}
void HouseCommandManager::TurnOnBathroomLight()
{
   std::cout << "Turn On Bathroom Light" << std::endl;
   ExecuteCommand(std::shared_ptr<ICommand>(new TurnOnLightCommand(&mHouse->mBathroomLight)));
}

void HouseCommandManager::TurnOffBathroomLight()
{
   std::cout << "Turn Off Bathroom Light" << std::endl;
   ExecuteCommand(std::shared_ptr<ICommand>(new TurnOffLightCommand(&mHouse->mBathroomLight)));
}

void HouseCommandManager::SetTemperature(double aTemperature)
{
   std::cout << "Set Temperature to " << aTemperature << std::endl;
   ExecuteCommand(std::shared_ptr<ICommand>(new SetTemperatureCommand(&mHouse->mThermostat, aTemperature)));
}

void HouseCommandManager::ExecuteCommand(std::shared_ptr<ICommand> aCommand)
{
   mRedoHistory.clear();
   aCommand->Execute();
   mUndoHistory.push_back(aCommand);
}

void HouseCommandManager::UndoCommand()
{
   if(mUndoHistory.size() > 0)
   {
      std::cout << "Undo" << std::endl;
      mUndoHistory.back()->UnExecute();
      mRedoHistory.push_back(mUndoHistory.back());
      mUndoHistory.pop_back();
   }
}

void HouseCommandManager::RedoCommand()                                                                                                                                                                                                 
{                                                              
   if(mRedoHistory.size() > 0)
   {
      std::cout << "Redo" << std::endl;
      mRedoHistory.back()->Execute();
      mUndoHistory.push_back(mRedoHistory.back());
      mRedoHistory.pop_back();
   }
}