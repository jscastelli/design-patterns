#ifndef THERMOSTAT_HPP
#define THERMOSTAT_HPP

class Thermostat
{
	public:
		Thermostat() = default;
		~Thermostat() = default;

		void SetTemperature(double aTemperature);

		const double& GetTemperature() const;

	private:

		double mTemperature{ 0.0 };
};

#endif