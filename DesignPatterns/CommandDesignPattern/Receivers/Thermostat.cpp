#include "Thermostat.hpp"

void Thermostat::SetTemperature(double aTemperature)
{
	mTemperature = aTemperature;
}

const double& Thermostat::GetTemperature() const
{
	return mTemperature;
}
