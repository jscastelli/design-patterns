#ifndef CONDIMENTBASEDECORATOR_HPP
#define CONDIMENTBASEDECORATOR_HPP

#include "../Beverages/BeverageBase.hpp"

//Condiment base class for which all condiments will derive from.
//Each derived class should, when implementing their Description() and Cost()
//methods, first call the CondimentBaseDecorator::Description()/Cost() methods first.
class CondimentBaseDecorator : public BeverageBase
{
   //Constructor(s) & Public Member Functions
   public:

      CondimentBaseDecorator(BeverageBase* aDecoratedBeverage);

      virtual std::string Description() override;

   //Protected Member Variables
   protected:

      BeverageBase* mDecoratedBeverage;
};

#endif // !CONDIMENTBASEDECORATOR_HPP