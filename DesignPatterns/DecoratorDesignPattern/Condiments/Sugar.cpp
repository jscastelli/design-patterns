#include "Sugar.hpp"

Sugar::Sugar(BeverageBase* aDecoratedBeverage) : CondimentBaseDecorator(aDecoratedBeverage)
{}

std::string Sugar::Description()
{
   return mDecoratedBeverage->Description() + CondimentBaseDecorator::Description() + "Sugar\n";
}

double Sugar::Cost()
{
   return mDecoratedBeverage->Cost() + 0.05;
}
