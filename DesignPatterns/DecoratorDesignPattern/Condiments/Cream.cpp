#include "Cream.hpp"

Cream::Cream(BeverageBase* aDecoratedBeverage) : CondimentBaseDecorator(aDecoratedBeverage)
{}

std::string Cream::Description()
{
   return mDecoratedBeverage->Description() + CondimentBaseDecorator::Description() + "Cream\n";
}

//Cream cost $0.10
double Cream::Cost()
{
   return mDecoratedBeverage->Cost() + 0.1;
}
