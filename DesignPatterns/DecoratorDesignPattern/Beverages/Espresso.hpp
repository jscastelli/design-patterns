#ifndef ESPRESSO_HPP
#define ESPRESSO_HPP

#include "../Beverages/BeverageBase.hpp"

class Espresso : public BeverageBase
{
   public:

      virtual std::string Description() override;
      virtual double Cost() override;
};

#endif // !ESPRESSO_HPP