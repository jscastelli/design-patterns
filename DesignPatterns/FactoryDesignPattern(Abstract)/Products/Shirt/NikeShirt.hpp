#ifndef NIKESHIRT_HPP
#define NIKESHIRT_HPP

#include "../Shirt/Shirt.hpp"

class NikeShirt : public Shirt
{
	public:

		virtual std::string Description();

};

#endif