#ifndef PANTS_HPP
#define PANTS_HPP

#include "../IClothing.hpp"

class Pants : public IClothing
{
	public:
		
		virtual std::string Description() { return "Generic Pants"; };
};

#endif