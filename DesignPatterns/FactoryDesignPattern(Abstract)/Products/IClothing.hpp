#ifndef ICLOTHING_HPP
#define ICLOTHING_HPP

#include <iostream>

class IClothing
{
	public:
		virtual std::string Description() = 0;
};

#endif