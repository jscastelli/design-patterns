#ifndef SHOES_HPP
#define SHOES_HPP

#include "../IClothing.hpp"

class Shoes : IClothing
{
	public:
		
		virtual std::string Description() { return "Generic Shoes"; };
};

#endif