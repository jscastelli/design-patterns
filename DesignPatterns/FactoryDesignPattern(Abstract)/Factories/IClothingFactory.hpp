#ifndef ICLOTHINGFACTORY_HPP
#define ICLOTHINGFACTORY_HPP

#include "../Products/Shoes/Shoes.hpp"
#include "../Products/Pants/Pants.hpp"
#include "../Products/Shirt/Shirt.hpp"

class IClothingFactory
{
	public:
		virtual Shoes* GetShoes() = 0;
		virtual Pants* GetPants() = 0;
		virtual Shirt* GetShirt() = 0;
};

#endif