#include "NikeFactory.hpp"

Shoes* NikeFactory::GetShoes()
{
   return new NikeShoes();
}

Pants* NikeFactory::GetPants()
{
   return new NikePants();
}

Shirt* NikeFactory::GetShirt()
{
   return new NikeShirt();
}
