#ifndef SINGLETON_HPP
#define SINGLETON_HPP

#include <iostream>

class Singleton
{
	public:
		
		~Singleton();

		static Singleton& GetInstance(std::string aName);

		void Display();

	private: 
		
		Singleton(std::string aName);

		Singleton(Singleton const&) = delete;
		void operator=(Singleton const&) = delete;

		std::string mName{ "" };
};

#endif