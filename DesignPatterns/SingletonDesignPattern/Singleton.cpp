#include "Singleton.hpp"

Singleton::Singleton(std::string aName)
{
	mName = aName;
	std::cout << "Singleton '" << mName << "' Created" << std::endl;
}

Singleton::~Singleton()
{
	std::cout << "Singleton Deleted" << std::endl;
}

Singleton& Singleton::GetInstance(std::string aName)
{
	//The singleton object is only created the first time GetInstance is called
	//Any additional calls to GetInstance return the originally created instance
	//of the Singleton object.
	static Singleton instance(aName);

	return instance;
}

void Singleton::Display()
{
	std::cout << "Singleton '"<< mName << "' is currently instantiated" << std::endl;
}