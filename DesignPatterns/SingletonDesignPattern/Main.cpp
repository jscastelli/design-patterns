#include "Singleton.hpp"

int main()
{
   //First call to GetInstance creates the Singleton object and names it 'first'
   Singleton& singleton1 = Singleton::GetInstance("first");
   singleton1.Display();

   //A second call to GetInstance returns the instance created above, therefor 'second'
   //is never created and the Display() method will display the same as it did above.
   Singleton& singleton2 = Singleton::GetInstance("second");
   singleton2.Display();
}