#include "AdvancedMediaPlayerAdapter.hpp"

void AdvancedMediaPlayerAdapter::PlayTrackFromTime(MediaFile* aTrack, double aTime)
{}

void AdvancedMediaPlayerAdapter::PlayNextTrack()
{}

void AdvancedMediaPlayerAdapter::PlayPreviousTrack()
{}

void AdvancedMediaPlayerAdapter::SkipForwardByAmountOfTime(double aAmountOfTime)
{}

void AdvancedMediaPlayerAdapter::SkipBackwardByAmountOfTime(double aAmountOfTime)
{}

void AdvancedMediaPlayerAdapter::AddTrackToQueue(MediaFile * aTrack)
{}

void AdvancedMediaPlayerAdapter::RemoveTrackFromQueue(MediaFile * aTrack)
{}
