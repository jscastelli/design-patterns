/******************************************************************************
*
*         @file: AdvancedMediaPlayerAdapter.hpp
*       @author: Joshua S. Castelli
*         @date:
*	     @brief:
*
******************************************************************************/
#ifndef ADVANCEDMEDIAPLAYERADAPTER_HPP 
#define ADVANCEDMEDIAPLAYERADAPTER_HPP

#include "../Adaptee/IAdvancedMediaPlayer.hpp"

/**
 * @class AdvancedMediaPlayerAdapter.hpp
 * @brief
 */

class AdvancedMediaPlayerAdapter : public IAdvancedMediaPlayer
{
	public:
		virtual void PlayTrackFromTime(MediaFile* aTrack, double aTime);
	   virtual void PlayNextTrack();
	   virtual void PlayPreviousTrack();
	   virtual void SkipForwardByAmountOfTime(double aAmountOfTime);
	   virtual void SkipBackwardByAmountOfTime(double aAmountOfTime);
	   virtual void AddTrackToQueue(MediaFile* aTrack);
	   virtual void RemoveTrackFromQueue(MediaFile* aTrack);
};

#endif