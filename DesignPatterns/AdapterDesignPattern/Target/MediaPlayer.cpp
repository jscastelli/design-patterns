/******************************************************************************
*
*         @file: MediaPlayer.hpp
*       @author: Joshua S. Castelli
*         @date:
*	     @brief:
*
******************************************************************************/
#ifndef MEDIAPLAYER_HPP 
#define MEDIAPLAYER_HPP

#include "ISimpleMediaPlayer.h"

/**
 * @class MediaPlayer.hpp
 * @brief
 */

class MediaPlayer : public ISimpleMediaPlayer
{
public:

	   MediaPlayer();
		~MediaPlayer();

		virtual void Play();
	   virtual void Next();
	   virtual void Previous();
	   virtual void AddSong();
	   virtual void RemoveSong();

private:

};

#endif