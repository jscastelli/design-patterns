/******************************************************************************
*
*         @file: ISimpleMediaPlayer.hpp
*       @author: Joshua S. Castelli
*         @date:
*	     @brief:
*
******************************************************************************/
#ifndef ISIMPLEMEDIAPLAYER_HPP 
#define ISIMPLEMEDIAPLAYER_HPP

#include "../Adaptee/MediaFile.hpp"

#include <queue>

/**
 * @class ISimpleMediaPlayer.hpp
 * @brief
 */

class ISimpleMediaPlayer
{
   public:

	   virtual void Play() = 0;
	   virtual void Next() = 0;
	   virtual void Previous() = 0;
	   virtual void AddSong(MediaFile* aSong) = 0;
	   virtual void RemoveSong(MediaFile* aSong) = 0;

	private:

		std::queue<MediaFile> mSongQueue{};
};

#endif