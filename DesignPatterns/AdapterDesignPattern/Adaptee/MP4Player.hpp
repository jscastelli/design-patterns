/******************************************************************************
*
*         @file: Mp4Player.hpp
*       @author: Joshua S. Castelli
*         @date:
*	     @brief:
*
******************************************************************************/
#ifndef MP4PLAYER_HPP 
#define MP4PLAYER_HPP

#include "IAdvancedMediaPlayer.hpp"

/**
 * @class Mp4Player.hpp
 * @brief
 */

class Mp4Player : IAdvancedMediaPlayer
{
public:
	Mp4Player();
	~Mp4Player();

private:

};

#endif