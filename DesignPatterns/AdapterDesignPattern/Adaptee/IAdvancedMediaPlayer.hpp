#ifndef IADVANCEDMEDIAPLAYER_HPP
#define IADVANCEDMEDIAPLAYER_HPP

#include "MediaFile.hpp"

#include <queue>

class IAdvancedMediaPlayer
{
   public:
	   
	   virtual void PlayTrackFromTime(MediaFile* aTrack, double aTime) = 0;
	   virtual void PlayNextTrack() = 0;
	   virtual void PlayPreviousTrack() = 0;
	   virtual void SkipForwardByAmountOfTime(double aAmountOfTime) = 0;
	   virtual void SkipBackwardByAmountOfTime(double aAmountOfTime) = 0;
	   virtual void AddTrackToQueue(MediaFile* aTrack) = 0;
	   virtual void RemoveTrackFromQueue(MediaFile* aTrack) = 0;

	private:

		std::queue<MediaFile*> mTrackQueue{};
};

#endif