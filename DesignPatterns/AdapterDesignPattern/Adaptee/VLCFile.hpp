/******************************************************************************
*
*         @file: VLCFile.hpp
*       @author: Joshua S. Castelli
*         @date:
*	     @brief:
*
******************************************************************************/
#ifndef VLCPLAYER_HPP 
#define VLCPLAYER_HPP

#include "MediaFile.hpp"

/**
 * @class VLCFile.hpp
 * @brief
 */

class VLCFile : public MediaFile
{
	//vlc file wrapper
};

#endif