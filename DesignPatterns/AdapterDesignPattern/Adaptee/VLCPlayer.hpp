/******************************************************************************
*
*         @file: VLCPlayer.hpp
*       @author: Joshua S. Castelli
*         @date:
*	     @brief:
*
******************************************************************************/
#ifndef VLCPLAYER_HPP 
#define VLCPLAYER_HPP

#include "IAdvancedMediaPlayer.hpp"

/**
 * @class VLCPlayer.hpp
 * @brief
 */

class VLCPlayer : public IAdvancedMediaPlayer
{
	public:
		VLCPlayer();
		~VLCPlayer();

};

#endif