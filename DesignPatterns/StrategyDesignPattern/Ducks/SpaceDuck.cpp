#include "SpaceDuck.hpp"
#include "../Behaviors/FlyWithRocket.hpp"
#include "../Behaviors/QuackMute.hpp"

//The constructor initializes Space Duck specific behavior for fly and Quack
SpaceDuck::SpaceDuck()
{
   mFlyBehavior = new FlyWithRocket(); //Space ducks fly with rockets
   mQuackBehavior = new QuackMute();   //There is no sound in space!
}
