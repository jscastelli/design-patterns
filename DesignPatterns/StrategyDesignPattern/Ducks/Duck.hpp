#ifndef DUCK_HPP
#define DUCK_HPP

#include "../Behaviors/FlyWithWings.hpp"
#include "../Behaviors/QuackQuack.hpp"

/* Duck is a base class that will be inherited by concrete classes
 * (i.e. Mallard, Rubber Duck, etc.)
 */
class Duck
{
   public:

      //Constructor
      Duck() {};

      //Destructor
      ~Duck();


   /* ~~~~~ Member Functions ~~~~~ */

      //Fly function
      void PerformFlyBehavior();

      //Quack function
      void PerformQuackBehavior();

      //Change behaviors during runtime
      void ChangeFlyBehavior(FlyBehaviorInterface* newFlyBehavior);
      void ChangeQuackBehavior(QuackBehaviorInterface* newQuackBehavior);

   /* ~~~~~ End Member Functions ~~~~~ */
   /* ================================ */

   
   /* ~~~~~ Member Variables ~~~~~ */
   protected:
      //Fly behavior variable
      FlyBehaviorInterface* mFlyBehavior{ nullptr };

      //Quack behavior variable
      QuackBehaviorInterface* mQuackBehavior{ nullptr };

   /* ~~~~~ End Member Variables ~~~~~ */
   /* ================================ */
};
#endif