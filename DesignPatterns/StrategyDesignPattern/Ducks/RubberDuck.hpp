#ifndef RUBBERDUCK_HPP
#define RUBBERDUCK_HPP

#include "Duck.hpp"

/* Rubber duck is a concrete class that inherits from Duck base class*/
class RubberDuck : public Duck
{
   public:
      RubberDuck();
};
#endif