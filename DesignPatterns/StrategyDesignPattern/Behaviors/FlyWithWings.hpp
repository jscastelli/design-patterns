#ifndef FLYWITHWINGS_HPP
#define FLYWITHWINGS_HPP

#include "FlyBehaviorInterface.hpp"

/* FlyWithWings is a concrete class that inherits from FlyBehaviorInterface
 * and provides functionality for objects that have the fly behavior. In this
 * case, the flying behavior will involve wings.
*/
class FlyWithWings : public FlyBehaviorInterface
{
   void Fly() override;
};
#endif