#ifndef FLYNOFLY_HPP
#define FLYNOFLY_HPP

#include "FlyBehaviorInterface.hpp"

/* FlyNoFly is a concrete class that inherits from FlyBehaviorInterface 
 * and provides functionality(or lack thereof) for objects that have the
 * fly behavior but don't actually fly. (i.e. Rubber Duck inherits from Duck,
 * so it inherits the mFlyBehavior variable. But rubber ducks don't fly, 
 * therefore it will use the FlyNoFly definition of Fly. You could also 
 * just opt out of defining functionality for mFlyBehavior in Rubber Duck's
 * constructor as Duck base class accounts for uninitialized behaviors, but
 * where's the fun in that?
*/
class FlyNoFly : public FlyBehaviorInterface
{
   void Fly() override;
};
#endif