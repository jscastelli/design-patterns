#ifndef QUACKQUACK_HPP
#define QUACKQUACK_HPP

#include "QuackBehaviorInterface.hpp"

/* QuackQuack is a concrete class that inherits from QuackBehaviorInterface
 * and provides functionality for objects that have the quack behavior. In this
 * case, the quacking behavior will be a normal quack.
*/
class QuackQuack : public QuackBehaviorInterface
{
   void Quack() override;
};
#endif