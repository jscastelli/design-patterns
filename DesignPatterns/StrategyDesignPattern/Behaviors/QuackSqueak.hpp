#ifndef QUACKSQUEAK_HPP
#define QUACKSQUEAK_HPP

#include "QuackBehaviorInterface.hpp"

/* QuackSqueak is a concrete class that inherits from QuackBehaviorInterface
 * and provides functionality for objects that have the quack behavior. In this
 * case, the quacking behavior will be a squeak.
*/
class QuackSqueak : public QuackBehaviorInterface
{
   void Quack() override;
};
#endif