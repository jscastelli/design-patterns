#ifndef FLYWITHROCKET_HPP
#define FLYWITHROCKET_HPP

#include "FlyBehaviorInterface.hpp"

/* FlyWithRocket is a concrete class that inherits from FlyBehaviorInterface
 * and provides functionality for objects that have the fly behavior. In this
 * case, the Flying behavior will involve rockets.
*/
class FlyWithRocket : public FlyBehaviorInterface
{
   void Fly() override;
};
#endif