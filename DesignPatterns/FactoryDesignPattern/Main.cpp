#include "Factories/RandomAnimalFactory.hpp"
#include "Factories/BalancedAnimalFactory.hpp"

// Main Entry Point
int main()
{
   std::cout << "======= Random Factory =======" << std::endl;

   RandomAnimalFactory randomAnimalFactory;
   randomAnimalFactory.CreateObject();
   randomAnimalFactory.CreateObject();
   randomAnimalFactory.CreateObject();
   randomAnimalFactory.CreateObject();
   randomAnimalFactory.CreateObject();
   randomAnimalFactory.CreateObject();
   randomAnimalFactory.CreateObject();
   randomAnimalFactory.CreateObject();
   randomAnimalFactory.CreateObject();
   randomAnimalFactory.CreateObject();

   std::cout << "==============================" << std::endl;
   std::cout << "====== Balanced Factory ======" << std::endl;

   BalancedAnimalFactory balancedAnimalFactory;
   balancedAnimalFactory.CreateObject();
   balancedAnimalFactory.CreateObject();
   balancedAnimalFactory.CreateObject();
   balancedAnimalFactory.CreateObject();
   balancedAnimalFactory.CreateObject();
   balancedAnimalFactory.CreateObject();
   balancedAnimalFactory.CreateObject();
   balancedAnimalFactory.CreateObject();
   balancedAnimalFactory.CreateObject();

   std::cout << "==============================" << std::endl;
}