#include "BalancedAnimalFactory.hpp"

#include <cstdlib>

Animal* BalancedAnimalFactory::CreateObject()
{
   Animal* balancedAnimal = nullptr;

   //Check if animal count is balanced
   bool balanced = true;
   int currentCount = mAnimalCount.begin()->second;
   for(auto&& item : mAnimalCount)
   {
      if(item.second != currentCount)
      {
         balanced = false;
      }
   }

   if(balanced)
   {
      //choose at random
      balancedAnimal = randomAnimalFactory.CreateObject();

      if(dynamic_cast<Dog*>(balancedAnimal) != nullptr)
      {
         ++mAnimalCount.find("dog")->second;
      }
      else if(dynamic_cast<Cat*>(balancedAnimal) != nullptr)
      {
         ++mAnimalCount.find("cat")->second;
      }
      else if(dynamic_cast<Bird*>(balancedAnimal) != nullptr)
      {
         ++mAnimalCount.find("bird")->second;
      }
      else
      {
         std::cout << "BalancedAnimalFactory: Failed to dynamic_cast to derived animal!" << std::endl;
      }
   }
   else
   {
      //unbalanced, therefor find the animal with the smallest population and create
      std::string animalType = mAnimalCount.begin()->first;
      int min = mAnimalCount.begin()->second;
      for(auto&& item : mAnimalCount)
      {
         if(item.second < min)
         {
            animalType = item.first;
            min = item.second;
         }
      }

      if(animalType == "dog")
      {
         balancedAnimal = new Dog();
         ++mAnimalCount.find("dog")->second;
         
      }
      else if(animalType == "cat")
      {
         balancedAnimal = new Cat();
         ++mAnimalCount.find("cat")->second;
      }
      else if(animalType == "bird")
      {
         balancedAnimal = new Bird();
         ++mAnimalCount.find("bird")->second;
      }
      else
      {
         std::cout << "BalancedAnimalFactory: Animal type not found!" << std::endl;
      }
   }

   PrintCurrentAnimalCount();

   return balancedAnimal;
}
