#ifndef RANDOMANIMALFACTORY_HPP
#define RANDOMANIMALFACTORY_HPP

//Interface
#include "IAnimalFactory.hpp"

//The animals to be created
#include "../Animals/Bird.hpp"
#include "../Animals/Cat.hpp"
#include "../Animals/Dog.hpp"

//STD
#include <map>

class RandomAnimalFactory : public IAnimalFactory
{
   public:
      
      RandomAnimalFactory();

      virtual Animal* CreateObject();

   private:
      
      int mRandom = 0;
};

#endif // !RANDOMANIMALFACTORY_HPP