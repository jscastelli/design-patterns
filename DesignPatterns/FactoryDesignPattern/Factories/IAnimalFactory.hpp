#ifndef IANIMALFACTORY_HPP
#define IANIMALFACTORY_HPP

#include "../Animals/Animal.hpp"

#include <map>

class IAnimalFactory
{
   public:
      
      virtual Animal* CreateObject() = 0;

   protected:
   
      void PrintCurrentAnimalCount()
      {
         for(auto&& animal : mAnimalCount)
         {
            std::cout << animal.first << ": " << animal.second << " || ";
         }

         std::cout << std::endl;
      }

   protected:

      std::map<std::string, int> mAnimalCount
      {
         {"dog", 0},
         {"cat", 0},
         {"bird", 0}
      };
};

#endif // !IFACTORY_HPP
