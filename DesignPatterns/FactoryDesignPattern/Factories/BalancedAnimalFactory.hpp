#ifndef BALANCEDANIMALFACTORY_HPP
#define BALANCEDANIMALFACTORY_HPP

//Interface
#include "IAnimalFactory.hpp"

//The animals to be created
#include "../Animals/Bird.hpp"
#include "../Animals/Cat.hpp"
#include "../Animals/Dog.hpp"

//Balanced factory, when already balanced, needs to randomly create an animal
#include "RandomAnimalFactory.hpp"

//STD
#include <vector>
#include <map>

class BalancedAnimalFactory : public IAnimalFactory
{
   public:

      virtual Animal* CreateObject();

   private:

      RandomAnimalFactory randomAnimalFactory;
};

#endif // !BALANCEDANIMALFACTORY_HPP