#ifndef BIRD_HPP
#define BIRD_HPP

#include "Animal.hpp"

class Bird : public Animal
{
   public:
      
      Bird();
      void Speak();
};

#endif // !BIRD_HPP