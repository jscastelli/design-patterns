#ifndef SUBSCRIBERTWO_HPP
#define SUBSCRIBERTWO_HPP

#include "SubscriberInterface.hpp"
#include "../Publisher/DataPublisher.hpp"

class SubscriberTwo : public SubscriberInterface
{
	public:

		SubscriberTwo(DataPublisher* aDataPublisher = nullptr);
		void Update(PublisherInterface* aPublisher);

	private:

		PublisherInterface* mPublisher{ nullptr };
};

#endif
