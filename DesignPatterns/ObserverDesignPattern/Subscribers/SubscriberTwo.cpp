#include "SubscriberTwo.hpp"
#include <iostream>

SubscriberTwo::SubscriberTwo(DataPublisher* aDataPublisher)
{
   if (aDataPublisher)
   {
      aDataPublisher->AddSubscriber(this);
      mPublisher = aDataPublisher;
   }
}

void SubscriberTwo::Update(PublisherInterface* aPublisher)
{
   DataPublisher* aDataPublisher = dynamic_cast<DataPublisher*>(aPublisher);
   if (aDataPublisher)
   {
      std::cout << "Subscriber 2 Updated" << std::endl;
      std::cout << "  Temperature: " << aDataPublisher->GetTemperature() << std::endl;
      std::cout << "     Humidity: " << aDataPublisher->GetHumidity() << std::endl;
   }
}
