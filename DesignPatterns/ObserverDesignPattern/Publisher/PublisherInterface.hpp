#ifndef PUBLISHERINTERFACE_HPP
#define PUBLISHERINTERFACE_HPP

#include <set>

class SubscriberInterface;

class PublisherInterface
{
   public:
      virtual void AddSubscriber(SubscriberInterface* aSubscriber) { mSubscribers.emplace(aSubscriber); };
      virtual void RemoveSubscriber(SubscriberInterface* aSubscriber) { mSubscribers.erase(aSubscriber); };
      virtual void NotifySubscribers() = 0;
      void DataChanged() { mDataChanged = true; }

   protected:
      std::set<SubscriberInterface*> mSubscribers = {};
      bool mDataChanged{ false };
};
#endif