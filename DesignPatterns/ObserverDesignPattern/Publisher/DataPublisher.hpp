#ifndef DATAPUBLISHER_HPP
#define DATAPUBLISHER_HPP

#include "PublisherInterface.hpp"

class DataPublisher : public PublisherInterface
{
   public:
      void NotifySubscribers();

      const float& GetTemperature() { return mTemperature; }
      const float& GetHumidity() { return mHumidity; }
      void SetTemperature(float aTemperature);
      void SetHumidity(float aHumidity);
      void SetData(float aTemperature, float aHumidity);

   private:
      float mTemperature{ 0.0 };
      float mHumidity{ 0.0 };
};
#endif