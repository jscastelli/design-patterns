#include "DataPublisher.hpp"
#include "../Subscribers/SubscriberInterface.hpp"

void DataPublisher::NotifySubscribers()
{
   if (mDataChanged)
   {
      for (SubscriberInterface* subscriber : mSubscribers)
      {
         subscriber->Update(this);
      }
      mDataChanged = false;
   }
}

void DataPublisher::SetTemperature(float aTemperature)
{
   if (mTemperature != aTemperature)
   {
      mTemperature = aTemperature;
      DataChanged();
      NotifySubscribers();
   }
}

void DataPublisher::SetHumidity(float aHumidity)
{
   if (mHumidity != aHumidity)
   {
      mHumidity = aHumidity;
      DataChanged();
      NotifySubscribers();
   }
}

void DataPublisher::SetData(float aTemperature, float aHumidity)
{
   if (mTemperature != aTemperature)
   {
      mTemperature = aTemperature;
      DataChanged();
   }

   if (mHumidity != aHumidity)
   {
      mHumidity = aHumidity;
      DataChanged();
   }

   NotifySubscribers();
}
