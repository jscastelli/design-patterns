#Add first-level subdirectories in the specified CURRENT_DIRECTORY to the specified SUBDIRECTORIES list.
macro(SUBDIRLIST SUBS CURRENT_DIRECTORY)
  file(GLOB FOUND RELATIVE ${CURRENT_DIRECTORY} ${CURRENT_DIRECTORY}/*)
  set(DIR_LIST "")
  foreach(CURRENT_FILE ${FOUND})
    if(IS_DIRECTORY ${CURRENT_DIRECTORY}/${CURRENT_FILE})
      list(APPEND DIR_LIST ${CURRENT_FILE})
    endif()
  endforeach()
  set(SUBS ${DIR_LIST})
endmacro()

#Same as above, but recursively parses the directory structure
macro(SUBDIRLIST_RECURSE SUBS_R CURRENT_DIRECTORY_R)
  file(GLOB_RECURSE FILES_R LIST_DIRECTORIES true RELATIVE ${CURRENT_DIRECTORY_R} ${CURRENT_DIRECTORY_R}/*)
  set(DIR_LIST_R "")
  foreach(CURRENT_FILE_R ${FILES_R})
    if(IS_DIRECTORY ${CURRENT_DIRECTORY_R}/${CURRENT_FILE_R})
      list(APPEND DIR_LIST_R ${CURRENT_FILE_R})
    endif()
  endforeach()
  set(SUBS_R ${DIR_LIST_R}) 
endmacro()

#Simply displays provided list content 
macro(LIST_ALL DISPLAY_LIST)
	foreach(item ${DISPLAY_LIST})
		message("\t" ${item}) 
	endforeach()
endmacro()

#Globs for c++ files in the provied list of directories
macro(GLOB_DIR_STRUCTURE SRC GLOB_LIST)
	file(GLOB MAIN_SRC ${CMAKE_CURRENT_SOURCE_DIR}/*.?pp)
	source_group(Main FILES ${MAIN_SRC})
	set(SRC_LIST ${MAIN_SRC})
	foreach(subdir ${GLOB_LIST})
		file(GLOB SOURCE_FILES ${CMAKE_CURRENT_SOURCE_DIR}/${subdir}/*.?pp)
		source_group(${subdir} FILES ${SOURCE_FILES})
		list(APPEND SRC_LIST ${SOURCE_FILES})
	endforeach()
	set(SRC ${SRC_LIST})
endmacro()


